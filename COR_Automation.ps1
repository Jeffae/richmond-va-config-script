﻿#City of Richmond - Semi Automated Configuration Script
#Script used to (somewhat) automate the configuration of COR devices


#Domain address
$global:domain = #RVA.gov domain
$global:domjoined = $false

#Prevent computer from sleeping
powercfg -change -standby-timeout-ac 0
powercfg -change -monitor-timeout-ac 0

#Change lid close to Do Nothing
powercfg -setacvalueindex 381b4222-f694-41f0-9685-ff5bb260df2e 4f971e89-eebd-4455-a8de-9e59040e7347 5ca83367-6e45-459f-a27b-476b1d01c936 0

#Clear Screen
clear

$global:domainuser = ""
$global:stage = ""

###Script Functions
#Function kickstarts the script
function StartScript {    
    SetCredentials
    SysInfo
    Menu
}

##Stage 1
#Function to connect to the VPN via the command line
function VPNConnect{
    taskkill /im vpnui.exe /f
    cd "C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client\"
    clear
    Write-Host "Reminder: Use yourPIN and Hardware Token for VPN Password!"
    .\vpncli.exe connect chvpn1.richmondgov.com
    Start-Sleep -Seconds 1
    $global:stage = "V"
    cd C:\
    clear
}

#Function to get credentials from the user, save the credentials in an XML file (encrypted), and store the user's username in a variable for later use
function SetCredentials {
    clear

    Write-Host "-------------------------------------------------------"
    Write-Host "Enter your RICHVA Domain Credentials for Authentication"
    Write-Host "-------------------------------------------------------`n`n"
    Write-Host "Do NOT add the domain to your username when logging in!`nEx: haesekej`n"

    $global:User = Read-Host -Prompt "Username"
    $UserD = $global:User + "@richva"
    $global:DomainUser = Get-Credential -Credential $UserD
}

##Stage 3
#Function to join the computer to the domain using the credentials collected
function DomainJoin {
    clear

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    Write-Host "Attempting to join '$global:sysname' to the domain '$global:domain'..."
    Add-Computer -DomainName $global:domain -Credential $global:DomainUser
    SysInfo

    if ($global:domjoined -eq $false) {
    Write-Output "`nIf the computer failed to join the domain:`n>>Check the VPN connection`n>>Verify your credentials`n>>Verify the computer name"
    }

    $prompt = Read-Host -Prompt "`n`nPress ENTER to restart WindowsNT, 'R' to retry, or 'M' to return to the menu..."

    #If true, advances the script state to 4 and restarts the computer
    if ($prompt -eq "r") {
        DomainJoin
    }

    #If true, returns to the menu
    elseif($prompt -eq "m"){
        $global:stage = "3"
        Menu
    }

    #If true, retries to join the domain
    else {
        $global:stage = "3"
        Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '3'
        shutdown -r -t 0
    }
}

##Stage 5
#Function to run the base install script from COR; the modified version of the script only removes 
#some minor annoyances (unnecessary powershell prompt, uses credentials collected from GetCredentials to authenticate)
function BaseInstall {
    
    clear

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    $prompt = Read-Host -prompt "Run modified COR base load? [Y/n]`n'm' to return to menu"

    #If true, the unmodified script from COR is run
    if ($prompt -eq "n"){
        "RVA DIT Network Resource Link"\NewDeviceLoad\runpost.bat #COR Script - New Device Load
        $global:stage = "5"
        Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '5'
    }

    #If true, returns to the menu
    elseif($prompt -eq "m"){
        Menu
    }

    #If true; runs the modified version of the COR base load script
    else{
        $global:stage = "5"
        Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '5'
        Add-Type -AssemblyName System.Windows.Forms
        Add-Type -AssemblyName System.Drawing

        $form = New-Object System.Windows.Forms.Form
        $form.Text = 'Please Select a New Device Post Install'
        $form.Size = New-Object System.Drawing.Size(500,300)
        $form.StartPosition = 'CenterScreen'

        $okButton = New-Object System.Windows.Forms.Button
        $okButton.Location = New-Object System.Drawing.Point(75,120)
        $okButton.Size = New-Object System.Drawing.Size(75,23)
        $okButton.Text = 'OK'
        $okButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
        $form.AcceptButton = $okButton
        $form.Controls.Add($okButton)

        $cancelButton = New-Object System.Windows.Forms.Button
        $cancelButton.Location = New-Object System.Drawing.Point(150,120)
        $cancelButton.Size = New-Object System.Drawing.Size(75,23)
        $cancelButton.Text = 'Cancel'
        $cancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
        $form.CancelButton = $cancelButton
        $form.Controls.Add($cancelButton)

        $label = New-Object System.Windows.Forms.Label
        $label.Location = New-Object System.Drawing.Point(10,20)
        $label.Size = New-Object System.Drawing.Size(280,20)
        $label.Text = 'Please select a New Device Post Install:'
        $form.Controls.Add($label)

        $listBox = New-Object System.Windows.Forms.ListBox
        $listBox.Location = New-Object System.Drawing.Point(10,40)
        $listBox.Size = New-Object System.Drawing.Size(280,20)
        $listBox.Height = 80

        [void] $listBox.Items.Add('Mobile')
        [void] $listBox.Items.Add('Desktop')
        [void] $listBox.Items.Add('CWA Base Load Mobile')
        [void] $listBox.Items.Add('CWA Base Load Desktop')
        [void] $listBox.Items.Add('DIT BASE Load')
        [void] $listBox.Items.Add('DIT SVC BASE Load')
        [void] $listBox.Items.Add('DJS Base Load Mobile')
        [void] $listBox.Items.Add('DJS Base Load Desktop')
        [void] $listBox.Items.Add('DPL Base Load Mobile')
        [void] $listBox.Items.Add('DPL Base Load Desktop')
        [void] $listBox.Items.Add('DSS Base Load Mobile')
        [void] $listBox.Items.Add('DPU Call Center')
        [void] $listBox.Items.Add('DSS Base Load Desktop')
        [void] $listBox.Items.Add('FIN Base Load Mobile')
        [void] $listBox.Items.Add('FIN Base Load Desktop')
        [void] $listBox.Items.Add('RFD Mobile')
        [void] $listBox.Items.Add('RFD Desktop')
        [void] $listBox.Items.Add('RSO Mobile')
        [void] $listBox.Items.Add('RSO Desktop')



        [void] $listBox.Items.Add('Test')

        $form.Controls.Add($listBox)

        $form.Topmost = $true

        $result = $form.ShowDialog()

        if ($result -eq [System.Windows.Forms.DialogResult]::OK)
        {
            $x = $listBox.SelectedItem
            $x
        }

        if  ($x -eq 'Mobile')
      
            {    
                #TODO: Mobile 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
         
            }
    
            ElseIf ($x -eq 'Desktop') 
            {    
                #TODO: Desktop 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait        
                start-process q:\installUpdate.bat -wait
        
         
            }
    
    
            ElseIf ($x -eq 'DPU Call Center') 
            {    
                #TODO: DPU Call Center
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-dpulinks.bat -wait
                start-process q:\software\install-callcenter-DPU-CIS.bat -wait
                start-process q:\software\install-cisco-ip-comm.bat -wait
                start-process q:\software\install-cisco-uni-wfo-mon-rec.bat -wait
                start-process q:\software\install-calabrio-cisco-uni-wforce.bat -wait
                start-process q:\software\install-cisco-finese.bat -wait
                start-process q:\software\munis.bat -wait
                start-process q:\software\Statemainframe.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
        
         
            }

            ElseIf ($x -eq 'DIT BASE Load') 
            {    
                #TODO: DIT BASE Load 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\installrsat.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\install-cisco-jabber.bat -wait
                start-process q:\software\install-sccmadmin.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
         
            }

            ElseIf ($x -eq 'DIT SVC BASE Load') 
            {    
                #TODO: DIT SVC BASE Load 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\installrsat.bat -wait
                start-process q:\software\install-ditsvclinks.bat -wait
                start-process q:\software\install-cisco-ip-comm.bat -wait
                start-process q:\software\install-cisco-uni-wfo-mon-rec.bat -wait
                start-process q:\software\install-calabrio-cisco-uni-wforce.bat -wait
                start-process q:\software\install-cisco-supervisor-desktop.bat -wait
                start-process q:\software\install-callcenter-DPU-CIS.bat -wait
                start-process q:\software\install-cisco-jabber.bat -wait
                start-process q:\software\install-sccmadmin.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait        
                start-process q:\installUpdate.bat -wait
         
            }

            ElseIf ($x -eq 'RFD Mobile') 
            {    
                #TODO: RFD Mobile 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installRFD-STDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
         
            }

            ElseIf ($x -eq 'RFD Desktop') 
            {    
                #TODO: RFD Desktop 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installRFD-STDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }

            ElseIf ($x -eq 'RSO Mobile') 
            {    
                #TODO: RSO Mobile 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installRSO-STDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait

                start-process q:\installUpdate.bat -wait
         
            }

            ElseIf ($x -eq 'RSO Desktop') 
            {    
                #TODO: RSO Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installRSO-STDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait

                start-process q:\installUpdate.bat -wait
         
            }
    
            ElseIf ($x -eq 'DJS Base Load Mobile') 
            {    
                #TODO: DJS Base Load Mobile 
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installDJS-STDSoft1.bat -wait
                start-process q:\installDJS-STDSoft2.bat -wait
                start-process q:\installDJS-STDSoft3.bat -wait
        
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }
        
            ElseIf ($x -eq 'DJS Base Load Desktop') 
            {    
                #TODO: DJS Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installDJS-STDSoft1.bat -wait
                start-process q:\installDJS-STDSoft2.bat -wait
                start-process q:\installDJS-STDSoft3.bat -wait
        
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }
        
            ElseIf ($x -eq 'CWA Base Load Mobile') 
            {    
                #TODO: CWA Base Load Mobile
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installCWA-STDAdmins.bat -wait        
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\installCWA-codecs.bat -wait
                start-process q:\software\installCWA-imats.bat -wait
                start-process q:\software\installCWA-statemainframe-scv.bat -wait
                start-process q:\software\installCWA-karpel.bat -wait
                start-process q:\software\installCWA-CIMS.bat -wait
                start-process q:\software\installCWA-casefinder.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installCWA-printers.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }
        
            ElseIf ($x -eq 'CWA Base Load Desktop') 
            {    
                #TODO: CWA Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installCWA-STDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\installCWA-codecs.bat -wait
                start-process q:\software\installCWA-imats.bat -wait
                start-process q:\software\installCWA-statemainframe-scv.bat -wait
                start-process q:\software\installCWA-karpel.bat -wait
                start-process q:\software\installCWA-CIMS.bat -wait
                start-process q:\software\installCWA-casefinder.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installCWA-printers.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }

            ElseIf ($x -eq 'FIN Base Load Mobile') 
            {    
                #TODO: CWA Base Load Mobile
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installFIN-STDAdmins.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }
        
            ElseIf ($x -eq 'FIN Base Load Desktop') 
            {    
                #TODO: CWA Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }

            
            ElseIf ($x -eq 'Test') 
            {    
                #TODO: TEST Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installSTDAdmins.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
            }
    
            ElseIf ($x -eq 'DSS Base Load Mobile')
      
            {    
                #TODO: DSS Base Load Mobile
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installDSS-STDAdmin.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
         
            }
    
            ElseIf ($x -eq 'DSS Base Load Desktop') 
            {    
                #TODO: DSS Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installDSS-STDAdmin.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait        
                start-process q:\installUpdate.bat -wait
        
         
            }
        
            ElseIf ($x -eq 'DPL Base Load Mobile')
      
            {    
                #TODO: DPL Base Load Mobile
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                #Start-Process Powershell
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installDPL-STDAdmin.bat -wait
                start-process q:\installVPN.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait
                start-process q:\installUpdate.bat -wait
         
            }
    
            ElseIf ($x -eq 'DPL Base Load Desktop') 
            {    
                #TODO: DPL Base Load Desktop
                #$tech = Read-Host 'What is the user name of Admin account for credential?'
                $cred = $global:DomainUser
                new-psdrive -name "q" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                new-psdrive -name "r" -root "RVA DIT Network Resource Link" -persist -psprovider "filesystem" -credential $cred
                start-process q:\installWIFI.bat -wait
                start-process q:\installDPL-STDAdmin.bat -wait
                start-process q:\software\install-silverlight.bat -wait
                start-process q:\software\install-.net_framework_4.8.bat -wait
                start-process q:\software\install-visualc++redis.bat -wait
                start-process q:\software\CleanInstallTeams.bat -wait
                start-process q:\software\install-sccmclient2002.bat -wait
                start-process q:\software\install-psremoteenable.bat -wait
                start-process q:\software\install-solarwindsagent.bat -wait
                start-process q:\installENS.bat -wait        
                start-process q:\installUpdate.bat -wait
        
         
            }
    }
    clear
    Write-Output "Base software installed!`n"
}

##Stage 7
#Function to perform a user profile migration from the COR server; the modified version of the script only shows 
#the profile file size and gives a rough estimation for how long for it to complete a transfer over the VPN
function ProfileTransfer {

    clear

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    $prompt = Read-Host -Prompt "Use modified profile transfer script? [Y/n]"

    #If true, the unmodofied script from COR is run
    if ($prompt -eq "n")
    {
        \\dit-hqdata01-pv\packages$\Software\NewDeviceLoad\profilexfer\restore64-local.bat #COR Script - VPN Profile Transfer
        $prompt2 = Read-Host -Prompt "Did the profile transfer complete successfully? [y/N]"

        #If true, the script state is advanced to stage 7
        if ($prompt2 -eq "y"){
            $global:stage = "7"
            Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '7'
        }

        #If true, returns to the menu
        elseif($prompt2 -eq "m"){
        Menu
        }

        #If true, retries the profile transfer
        else{
        ProfileTransfer
        }
    }

    #If true, runs the modified profile transfer script
    else {
    $PCName = Read-Host -Prompt "Enter the PC name of the captured device (old device name)"
    $UName = Read-Host -Prompt "Enter the username of the profile to transfer"

    #Gets file size and does math to get a rough ETA
    $File = Get-ChildItem "RVA DIT Network Resource Link"\$PCName\USMT\USMT.MIG
    $Size = ($File | Measure-Object -Sum Length).Sum / 1GB
    $EtaH = ((((($Size * 1000) * 8) / 15) / 60) / 60)

    $Eta = [math]::Round($EtaH,1) #Rounds to the nearest tenths decimal
    $Size = [math]::Round($Size,2) #Rounds to the nearest hundreths decimal

    if($size -eq 0){
        $prompt = Read-Host -Prompt "Profile may not exist on server, would you like to abort the transfer? [y/N]"
        if ($prompt -eq "y"){
            Menu
        }
    }

    Read-Host -Prompt "`nProfile size (in GB): $Size `nAverage download speed: 15 Mbit`nApproximate ETA: $Eta hours `nPress anykey to continue`n"


    #Begins copying USMT files and the user's profile to the device
    xcopy "RVA DIT Network Resource Link"\profilexfer\win10usmtamd64\*.* /s /e /v /c /h /k /y c:\windows\ccmcache\usmt\amd64\
    md c:\windows\ccmcache\usmt\$PCName\
    xcopy "RVA DIT Network Resource Link"\$PCName\*.* /s /e /v /c /h /k /y c:\windows\ccmcache\usmt\$PCName\
    Write-Output "`n`nProfile size (in GB): $Size `nAverage download speed: 15 Mbit`nApproximate ETA: $Eta hours `nPress anykey to continue`n"
    cd C:\windows\ccmcache\usmt\amd64\
    .\loadstate.exe c:\windows\ccmcache\usmt\$PCName /i:c:\windows\ccmcache\usmt\amd64\migapp.xml /i:c:\windows\ccmcache\usmt\amd64\migdocs.xml /i:c:\windows\ccmcache\usmt\amd64\miguser.xml /ui:richva\$Uname /ue:$PCName\* /ue:richva\* /c

    $prompt2 = Read-Host -Prompt "Did the profile transfer complete successfully? [y/N]"

    #If true, advances the script stage to 7
    if ($prompt2 -eq "y"){
        $global:stage = "7"
        Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '7'
    }

    #If true, returns to the menu
    elseif($prompt2 -eq "m"){
        Menu
        }

    #If true, retries the profile transfer
    else{
        ProfileTransfer
        }
    }   
}

##Stage 1
#Function installs the VPN software and runs the VPNConnect function
function VPNInstall {
    C:\COR_ConfigScript\anyconnect-win-4.4.01054-core-vpn-webdeploy-k9.exe /passive
    clear
    Write-Output "Installing Sisko AnyConnect - Please Wait!"
    Start-Sleep -Seconds 10
    xcopy C:\COR_ConfigScript\CORAutomation.lnk C:\Users\Administrator\Desktop
    cd C:\
    clear
    $global:stage = "1"
    Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '1' | Select -Index 0
    VPNConnect
}

##Stage 2
#Function changes the local CORUser password
function ChangeLocalPW {
    clear
    $LocalPW = ConvertTo-SecureString "DellTig1" -AsPlainText -Force
    $LocalAct = Get-LocalUser -Name "CORUser"
    $LocalAct | Set-LocalUser -Password $LocalPW
    $global:stage = "2"
    Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '2'
    Write-Output "CORUser Password Changed!`n"
    Read-Host -Prompt "Press anykey to continue"
}

##Stage 6
#Function to install additional software specified by the CAF
function AdditionalSW {
    clear

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    $prompt = Read-Host -Prompt "Open COR Software Menu? [Y/n]"
    if ($prompt -eq "n") {
        clear
    }
    else {
        \\dit-hqdata01-pv\packages$\Software\NewDeviceLoad\SoftwareMenu.bat #COR Script - Software Menu
        clear
    }
    $prompt = Read-Host -Prompt "Open 'List of Software' spreadsheet? [y/N]"
    if ($prompt -eq "y"){
        explorer "C:\COR_ConfigScript\List of software.xlsx"
    }
    $global:stage = "6"
    Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '6'
}

##Stage 4
#Function to update group policy on the device after joining the domain; the process is executed as the user running the 
#script so the user's domain profile is available to be used to log into the device before it is connected to the VPN; also,
#a shortcut to run the script is created on the user's desktop and the old shortcut on the admin desktop is deleted
function UpdateGP {
    clear

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -ne "c") {
            Menu
        }
    }

    if ($global:domjoined -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the RICHVA domain! Press C to Continue, else ENTER to return to menu"
        if ($prompt -ne "c") {
            Menu
        }
    }

    Write-Output "Syncronizing time with Domain Controller"
    w32tm /resync /force
    Write-Output "Updating group policy, please wait for the foreground window to close before proceeding..."
    Start-Process gpupdate -Credential $global:DomainUser
    Timeout 45
    $prompt = Read-Host -Prompt "Press ENTER to restart, R to try again, or M to go to the Menu"
    if ($prompt -eq "r"){
        UpdateGP
    }
    elseif($prompt -eq "m"){
        Menu
    }
    else{
        xcopy C:\COR_ConfigScript\CORAutomation.lnk C:\Users\CORUser\Desktop\
        xcopy C:\COR_ConfigScript\CORAutomation.lnk C:\Users\$global:user\Desktop\
        Remove-Item C:\Users\Administrator\Desktop\CORAutomation.lnk
        $global:stage = "4"
        Set-Content -Path 'C:\COR_ConfigScript\ScriptState.txt' -Value '4'
        shutdown -r -t 0 
    }
}

##Stage 8
#Function to clean script files off the device after the script has completed; function deletes shortcuts, 
#credentials file, state file, and attempts to confirm that the files were deleted; also, function sets the
#devices power settings back to default
function Clean {
    clear
    Write-Output "Cleaning script files..."
    Remove-Item -Recurse C:\COR_ConfigScript
    Write-Output "Saved user"
    Remove-Item C:\users\$global:user\Desktop\CORAutomation.lnk
    clear
            
    if (Test-Path C:\COR_ConfigScript\Credentials.xml -PathType Leaf){
        Write-Output "Credentials.xml was not deleted!! (C:\COR_ConfigScript\Credentials.xml)"
        Read-Host -Prompt "Manually delete Credentials file before proceeding! `nPress ENTER to continue..."
    }

    Write-Output "Changing power settings back to defaults..."
    powercfg -change -standby-timeout-ac 30
    powercfg -change -monitor-timeout-ac 10

}

#Function to start function Clean, shuts down the PC if prompted, or exits the script
function ExitScript{
    Clean
    $Prompt = Read-Host -prompt "Press s to shutdown WindowsNT or press Enter to close this window."

    if ($Prompt -eq "s") {
        shutdown -s -t 0
    }
    else{
    break outer
    }
}

#function to rename the PC if there was a name change or an erroronious name was entered during initial OOBE
function RenamePC{

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    Color "DarkRed" "White"
    clear
    $currName = $env:computername
    Write-Host "The current computer name is: $currName"
    $prompt = Read-Host -Prompt "Do you want to rename this computer? [y/N]"

    #If true, changes the computer name to the desired name
    if ($prompt -eq "y"){
        Write-Host "`nThe computer will need to be restarted after changing the computer name!"
        $newName = Read-Host -Prompt "Enter the new computer name"
        Rename-Computer -NewName $newName -DomainCredential $global:DomainUser
        $prompt2 = Read-Host -Prompt "Restart now? [y/N]"

        #If true, restarts the computer
        if ($prompt2 -eq "y"){
            Write-Output "Restarting in 10 seconds..."
            shutdown -r -t 10
       }
    }

    #If true, reminds user to reboot and returns to menu
    else{
        Start-Sleep -Seconds 3
        Menu
    }

    #If true, returns to menu
    else{
    Write-Output "Cancelling name change.."
    Start-Sleep -Seconds 1
    Menu
    }
}

#Function to change the domain user's password
function ChangeDomainPW {

    if ($global:VPNStatus -eq $false) {
        $prompt = Read-Host -Prompt "Confirm the device is connected to the VPN! Press C to continue, else ENTER to return to menu"
        if ($prompt -eq "c") {
            exit
        }
        else {
            Menu
        }
    }

    function Header {
        Write-Output "-----------------------------`nChange RICHVA Domain Password`n-----------------------------`n`n"
        Write-Output "Remember to connect to the VPN before changing your password!`n`n"
    }
    Color "DarkBlue" "White"
    clear

    Header
    $Prompt = Read-Host -Prompt "Do you want to change your domain account password? [y/N]"
    if ($Prompt -eq 'y') {
        clear
        Header

        Set-ADAccountPassword -Identity $global:user
        $global:stage = "P"
        Read-Host -Prompt "Press anykey to continue"
    }
    else {
        Menu
    }
    
}

#Functon to show system information, such as computer name and domain status
function SysInfo {
    $global:sysinfo = Get-WmiObject -Class Win32_ComputerSystem
    $global:sysname = $sysinfo.name
    $global:sysdomain = $sysinfo.domain

    if ($global:sysdomain -eq "richva.ci.richmond.va.us") {
        [bool]$global:domjoined = $true
    }
}

#Function to set the background and foreground color of the Powershell window
function Color ($bc,$fc) {
    $a = (Get-Host).UI.RawUI
    $a.BackgroundColor = $bc
    $a.ForegroundColor = $fc ; clear
}

#function to detect VPN connection status
function VPNStatus {
    $VPNConnectionStatus = Get-NetAdapter -InterfaceDescription "Cisco*" | where {$_.status -eq "Up"}
    if ($VPNConnectionStatus -eq $null) {
        [bool]$Global:VpnStatus = $false
        }
    else {
        [bool]$global:VPNStatus = $true
        }
}


function Restart {
    $prompt = Read-Host -Prompt "Are you sure you want to restart $global:sysnme [y/N]"

    if ($prompt -eq "y"){
        shutdown -r -t 0
    }
    else {Menu}
}

function Shutdown {
    $prompt = Read-Host -Prompt "Are you sure you want to shutdown $global:sysnme [y/N]"

    if ($prompt -eq "y"){
        shutdown -s -t 0
    }
    else {Menu}
}

#Function to display the menu and take user input to make a selection
function Menu {
    
    Color "Black" "White"
    switch ($global:stage) {
    "1" {$step = "1 - Install VPN"}
    "2" {$step = "2 - Change COR User Password"}
    "3" {$step = "3 - Join Computer to Domain"}
    "4" {$step = "4 - Update Group Policy"}
    "5" {$step = "5 - Install Base Load"}
    "6" {$step = "6 - Install Additional Software"}
    "7" {$step = "7 - Profile Transfer"}
    "V" {$step = "V - Connected to VPN"}
    "P" {$step = "P - Changed Domain Account Password"}
    }
    Write-Output "Loading - Please wait..."
    VPNStatus

    Clear
    Write-Output "COR Configuration Script - $global:sysname"
    Write-Output "--------------------------------------------"
    Write-Output "Curent Credentials: $global:user"
    Write-Output "Joined to domain: $global:domjoined"
    Write-Output "Connected to VPN: $global:VPNStatus"

    if ($step -ne $null) {
        Write-Output "Last Run Command: $step"
    }

    Write-Output "--------------------------------------------`n"
    Write-Output "1 - Install VPN"
    Write-Output "2 - Change CORUser Password"
    Write-Output "3 - Join Computer to RICHVA Domain"
    Write-Output "4 - Update Group Policy"
    Write-Output "5 - Base Software Install"
    Write-Output "-----------------------------------"
    Write-Output "6 - Additional Software Install"
    Write-Output "7 - Profile Transfer"
    Write-Output "8 - Remove Script Files and Exit"
    Write-Output "-----------------------------------"
    Write-Output "C - Change Credentials"
    Write-Output "V - Connect to VPN" 
    Write-Output "N - Change PC Name"
    Write-Output "P - Change Domain User Password"
    Write-Output "-----------------------------------"
    Write-Output "R - Restart PC"
    Write-Output "S - Shutdown PC"
    Write-Output "`n`n"

    if ((get-date).day -lt 7) {
        Write-Output "-----------------------------------"
        Write-Output "It's the begining of the month. `nDon't forget to change your password!"
        Write-Output "-----------------------------------`n`n"
    }

    $selection = Read-Host -Prompt ">>Make a selection"

    #Switch-Case to select a stage to start
    switch ($selection) {
        "1" {VPNInstall}
        "2" {ChangeLocalPW}
        "3" {DomainJoin}
        "4" {UpdateGP}
        "5" {BaseInstall}
        "6" {AdditionalSW}
        "7" {ProfileTransfer}
        "8" {ExitScript}
        "c" {SetCredentials}
        "v" {VPNConnect}
        "n" {RenamePC}
        "p" {ChangeDomainPW}
        "r" {Restart}
        "s" {Shutdown}
    }
    Menu
}

#Starts the functon StartScript to kickstart this maze of scripting
StartScript